package com.markomapp.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.markomapp.models.Employee;
import com.markomapp.repositories.EmployeeRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEmployee {
	
	@Autowired
	private EmployeeRepo employeeRepo;
	
	@GetMapping("/employee")
    public ResponseEntity<List<Employee>> GetAllEmployee() {
        try {
            List<Employee> employee = this.employeeRepo.findAll();
            return new ResponseEntity<>(employee, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
	
	@GetMapping("/employee/{id}")
    public ResponseEntity<List<Employee>> GetEmployeeById(@PathVariable("id") Long id) {
        try {
            Optional<Employee> employee = this.employeeRepo.findById(id);
            if (employee.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(employee, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchemployee/{keyword}")
    public ResponseEntity<List<Employee>> SearchEmployeebyCode(@PathVariable("keyword") String keyword) {
        if (keyword != null) {
            List<Employee> employee = this.employeeRepo.SearchEmployee(keyword);
            return new ResponseEntity<>(employee, HttpStatus.OK);
        } else {
            List<Employee> employee = this.employeeRepo.findAll();
            return new ResponseEntity<>(employee, HttpStatus.OK);
        }
    }

    @PostMapping("/employee")
    public ResponseEntity<Object> SaveEmployee(@RequestBody Employee employee) {
        try {
            employee.setCreatedBy("Nando");
            employee.setCreatedOn(new Date());
            this.employeeRepo.save(employee);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<Object> UpdateEmployee(@RequestBody Employee employee, @PathVariable("id") Long id) {
        Optional<Employee> employeeData = this.employeeRepo.findById(id);

        if (employeeData.isPresent()) {
            employee.setId(id);
            employee.setModifiedBy("Nando");
            employee.setModifiedOn(new Date());
            this.employeeRepo.save(employee);
            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/employee/{id}")
    public ResponseEntity<Object> DeleteEmployee(@PathVariable("id") Long id) {
        this.employeeRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }
	
}
