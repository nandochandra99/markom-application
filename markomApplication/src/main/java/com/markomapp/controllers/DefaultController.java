package com.markomapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/")
public class DefaultController {
	
	@GetMapping(value = "index")
	public ModelAndView index() {
        ModelAndView view = new ModelAndView("index");
        return view;
    }
	
	@GetMapping(value = "user")
	public ModelAndView user() {
        ModelAndView view = new ModelAndView("user");
        return view;
    }
	
	@GetMapping(value = "event")
	public ModelAndView event() {
        ModelAndView view = new ModelAndView("event");
        return view;
    }
	
}
