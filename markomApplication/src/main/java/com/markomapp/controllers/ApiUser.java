package com.markomapp.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.markomapp.models.User;
import com.markomapp.repositories.UserRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiUser {
	
	@Autowired
    private UserRepo userRepo;

    @GetMapping("/user")
    public ResponseEntity<List<User>> GetAllUser() {
        try {
            List<User> user = this.userRepo.findAll();
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    
    @GetMapping("/user/{id}")
    public ResponseEntity<List<User>> GetUserId(@PathVariable("id") Long id) {
        try {
            Optional<User> user = this.userRepo.findById(id);
            if (user.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(user, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/searchuser/{keyword}")
    public ResponseEntity<List<User>> SearchUser(@PathVariable("keyword") String keyword) {
        if (keyword != null) {
            List<User> user = this.userRepo.SearchUser(keyword);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            List<User> user = this.userRepo.findAll();
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @PostMapping("/user")
    public ResponseEntity<Object> SaveUser(@RequestBody User user) {
        try {
            user.setCreatedBy("Nando");
            user.setCreatedOn(new Date());
            this.userRepo.save(user);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<Object> UpdateUser(@RequestBody User user, @PathVariable("id") Long id) {
        Optional<User> userData = this.userRepo.findById(id);

        if (userData.isPresent()) {
        	user.setId(id);
        	user.setModifiedBy("Nando");
        	user.setModifiedOn(new Date());
            this.userRepo.save(user);
            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<Object> DeleteVariant(@PathVariable("id") Long id) {
        this.userRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }
	
}
