package com.markomapp.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.markomapp.models.Event;
import com.markomapp.repositories.EventRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEvent {
	
	@Autowired
    private EventRepo eventRepo;

    @GetMapping("/event")
    public ResponseEntity<List<Event>> GetAllVariant() {
        try {
            List<Event> event = this.eventRepo.findAll();
            return new ResponseEntity<>(event, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    
    @GetMapping("/event/{id}")
    public ResponseEntity<List<Event>> GetEventId(@PathVariable("id") Long id) {
        try {
            Optional<Event> event = this.eventRepo.findById(id);
            if (event.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(event, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    
    @GetMapping("/searchevent/{keyword}")
    public ResponseEntity<List<Event>> SearchEventName(@PathVariable("keyword") String keyword) {
    	System.out.println(keyword);
        if (keyword != null) {
            List<Event> event = this.eventRepo.SearchEvent(keyword);
            return new ResponseEntity<>(event, HttpStatus.OK);
        } else {
            List<Event> event = this.eventRepo.findAll();
            return new ResponseEntity<>(event, HttpStatus.OK);
        }
    }

    @PostMapping("/event")
    public ResponseEntity<Object> SaveEvent(@RequestBody Event event) {
        try {
        	event.setCreatedBy("Nando");
        	event.setCreatedOn(new Date());
            this.eventRepo.save(event);
            return new ResponseEntity<>("success", HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/event/{id}")
    public ResponseEntity<Object> UpdateEvent(@RequestBody Event event, @PathVariable("id") Long id) {
        Optional<Event> eventData = this.eventRepo.findById(id);

        if (eventData.isPresent()) {
        	event.setId(id);
        	event.setModifiedBy("Nando");
        	event.setModifiedOn(new Date());
            this.eventRepo.save(event);
            ResponseEntity rest = new ResponseEntity<>("success", HttpStatus.OK);
            return rest;
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/event/{id}")
    public ResponseEntity<Object> DeleteVariant(@PathVariable("id") Long id) {
        this.eventRepo.deleteById(id);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }
	
}
