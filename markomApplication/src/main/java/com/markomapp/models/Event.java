package com.markomapp.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_evet")
public class Event extends Common {
	
	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long Id;

	@Column(name = "code", nullable = false)
    private String Code;
	
	@Column(name = "event_name", nullable = false)
    private String EventName;
	
	@Column(name = "start_date", nullable = false)
    private Date StartDate;
	
	@Column(name = "end_date", nullable = false)
    private Date EndDate;
	
	@Column(name = "place", nullable = false)
    private String Place;
	
	@Column(name = "budget", nullable = false)
    private Long Budget;
	
	@ManyToOne
    @JoinColumn(name = "request_by", insertable = false, updatable = false)
    public User user;

    @Column(name = "request_by", nullable = true)
    private Long RequestBy;
	
	@ManyToOne
    @JoinColumn(name = "assign_to", insertable = false, updatable = false)
    public Employee employee;

    @Column(name = "assign_to", nullable = true)
    private Long AssignTo;
    

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getEventName() {
		return EventName;
	}

	public void setEventName(String eventName) {
		EventName = eventName;
	}

	public Date getStartDate() {
		return StartDate;
	}

	public void setStartDate(Date startDate) {
		StartDate = startDate;
	}

	public Date getEndDate() {
		return EndDate;
	}

	public void setEndDate(Date endDate) {
		EndDate = endDate;
	}

	public String getPlace() {
		return Place;
	}

	public void setPlace(String place) {
		Place = place;
	}

	public Long getBudget() {
		return Budget;
	}

	public void setBudget(Long budget) {
		Budget = budget;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getRequestBy() {
		return RequestBy;
	}

	public void setRequestBy(Long requestBy) {
		RequestBy = requestBy;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Long getAssignTo() {
		return AssignTo;
	}

	public void setAssignTo(Long assignTo) {
		AssignTo = assignTo;
	}
	
}
