package com.markomapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarkomApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarkomApplication.class, args);
	}

}
