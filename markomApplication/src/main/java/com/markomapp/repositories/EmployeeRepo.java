package com.markomapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.markomapp.models.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
	
	@Query(value = "SELECT * FROM m_employee WHERE lower(employee_code) LIKE lower(concat('%', ?1, '%'))", nativeQuery = true)
	List<Employee> SearchEmployee(String keyword);

}
