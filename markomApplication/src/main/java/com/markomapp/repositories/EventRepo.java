package com.markomapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.markomapp.models.Event;

public interface EventRepo extends JpaRepository<Event, Long> {
	
	@Query(value = "SELECT * FROM t_evet WHERE lower(event_name) LIKE lower(concat('%', ?1, '%'))", nativeQuery = true)
	List<Event> SearchEvent(String keyword);
	
}
