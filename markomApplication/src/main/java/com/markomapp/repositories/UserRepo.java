package com.markomapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.markomapp.models.User;

public interface UserRepo extends JpaRepository<User, Long> {
	
	@Query(value = "SELECT * FROM m_user WHERE lower(username) LIKE lower(concat('%', ?1, '%'))", nativeQuery = true)
	List<User> SearchUser(String keyword);
	
}
